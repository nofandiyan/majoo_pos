package main

import (
	"fmt"
	"log"
	"net/http"
	"strings"

	"majoo_pos/app/products"
	"majoo_pos/app/users"
	"majoo_pos/database"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

func main() {
	database.Conn()
	handleRequests()
}

func handleRequests() {

	// myRouter := mux.NewRouter().StrictSlash(true)
	router := gin.Default()

	//login
	router.POST("/api/users/login", users.LoginPage)

	//user
	router.GET("/api/users", auth, users.UserPage)
	router.POST("/api/users/create", auth, users.CreateUser)
	router.POST("/api/users/get", auth, users.GetUsers)
	router.POST("/api/users/getby", auth, users.GetUser)
	router.PUT("/api/users/update", auth, users.UpdateUser)
	router.DELETE("/api/users/delete", auth, users.DeleteUser)

	//product
	router.GET("/api/products", auth, products.ProductPage)
	router.POST("/api/products/create", auth, products.CreateProduct)
	router.POST("/api/products/get", auth, products.GetProducts)
	router.POST("/api/products/getby", auth, products.GetProduct)
	router.PUT("/api/products/update", auth, products.UpdateProduct)
	router.DELETE("/api/products/delete", auth, products.DeleteProduct)

	log.Fatal(http.ListenAndServe(":9999", router))
}

//auth
func auth(c *gin.Context) {
	tokenString := ""

	//token authorization
	bearToken := c.Request.Header.Get("Authorization")
	strArr := strings.Split(bearToken, " ")
	if len(strArr) == 2 {
		tokenString = strArr[1]
	}

	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if jwt.GetSigningMethod("HS256") != token.Method {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}

		return []byte("secret"), nil
	})

	if token != nil && err == nil {
		fmt.Println("token verified")
	} else {
		result := gin.H{
			"message": "not authorized",
			"error":   err.Error(),
		}
		c.JSON(http.StatusUnauthorized, result)
		c.Abort()
	}
}
