package database

import (
	"log"

	"github.com/jinzhu/gorm"
)

var DB *gorm.DB
var err error

func Conn() {
	DB, err = gorm.Open("mysql", "root:@tcp(127.0.0.1:3306)/majoo_pos?charset=utf8&parseTime=True")

	if err != nil {
		log.Println("Connection Failed to Open", err)
	} else {
		log.Println("Connection Success")
	}
}
