-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.19-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.2.0.6213
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for majoo_pos
CREATE DATABASE IF NOT EXISTS `majoo_pos` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `majoo_pos`;

-- Dumping structure for table majoo_pos.m_cities
CREATE TABLE IF NOT EXISTS `m_cities` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `city_name` varchar(50) NOT NULL,
  `prov_id` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_m_cities_m_provinces` (`prov_id`),
  CONSTRAINT `FK_m_cities_m_provinces` FOREIGN KEY (`prov_id`) REFERENCES `m_provinces` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table majoo_pos.m_cities: ~2 rows (approximately)
/*!40000 ALTER TABLE `m_cities` DISABLE KEYS */;
INSERT INTO `m_cities` (`id`, `city_name`, `prov_id`) VALUES
	(1, 'Malang', 1),
	(2, 'Madiun', 1),
	(3, 'Surakarta', 2);
/*!40000 ALTER TABLE `m_cities` ENABLE KEYS */;

-- Dumping structure for table majoo_pos.m_districts
CREATE TABLE IF NOT EXISTS `m_districts` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `disctricts_name` varchar(50) NOT NULL,
  `city_id` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_m_districts_m_cities` (`city_id`),
  CONSTRAINT `FK_m_districts_m_cities` FOREIGN KEY (`city_id`) REFERENCES `m_cities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table majoo_pos.m_districts: ~2 rows (approximately)
/*!40000 ALTER TABLE `m_districts` DISABLE KEYS */;
INSERT INTO `m_districts` (`id`, `disctricts_name`, `city_id`) VALUES
	(1, 'Kartoharjo', 2),
	(2, 'Lowokwaru', 1);
/*!40000 ALTER TABLE `m_districts` ENABLE KEYS */;

-- Dumping structure for table majoo_pos.m_merchants
CREATE TABLE IF NOT EXISTS `m_merchants` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nik` int(25) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `address` varchar(100) NOT NULL,
  `disctricts_id` int(10) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `user_id` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_m_merchants_m_districts` (`disctricts_id`),
  KEY `FK_m_merchants_m_users` (`user_id`),
  CONSTRAINT `FK_m_merchants_m_districts` FOREIGN KEY (`disctricts_id`) REFERENCES `m_districts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_m_merchants_m_users` FOREIGN KEY (`user_id`) REFERENCES `m_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table majoo_pos.m_merchants: ~0 rows (approximately)
/*!40000 ALTER TABLE `m_merchants` DISABLE KEYS */;
INSERT INTO `m_merchants` (`id`, `nik`, `name`, `address`, `disctricts_id`, `phone`, `user_id`) VALUES
	(1, 2147483647, 'Budi Eko', 'Jl. Kenangan', 2, '081234567891', 2);
/*!40000 ALTER TABLE `m_merchants` ENABLE KEYS */;

-- Dumping structure for table majoo_pos.m_outlets
CREATE TABLE IF NOT EXISTS `m_outlets` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `outlet_name` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `disctricts_id` int(10) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `merchant_id` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_m_outlets_m_districts` (`disctricts_id`),
  KEY `FK_m_outlets_m_merchants` (`merchant_id`),
  CONSTRAINT `FK_m_outlets_m_districts` FOREIGN KEY (`disctricts_id`) REFERENCES `m_districts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_m_outlets_m_merchants` FOREIGN KEY (`merchant_id`) REFERENCES `m_merchants` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table majoo_pos.m_outlets: ~0 rows (approximately)
/*!40000 ALTER TABLE `m_outlets` DISABLE KEYS */;
INSERT INTO `m_outlets` (`id`, `outlet_name`, `address`, `disctricts_id`, `phone`, `merchant_id`) VALUES
	(1, 'Cabang Lowokwaru', 'Jl. Kenangan', 2, '081234567892', 1);
/*!40000 ALTER TABLE `m_outlets` ENABLE KEYS */;

-- Dumping structure for table majoo_pos.m_path
CREATE TABLE IF NOT EXISTS `m_path` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `path` varchar(150) NOT NULL,
  `product_id` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_m_path_m_products` (`product_id`),
  CONSTRAINT `FK_m_path_m_products` FOREIGN KEY (`product_id`) REFERENCES `m_products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Dumping data for table majoo_pos.m_path: ~0 rows (approximately)
/*!40000 ALTER TABLE `m_path` DISABLE KEYS */;
/*!40000 ALTER TABLE `m_path` ENABLE KEYS */;

-- Dumping structure for table majoo_pos.m_products
CREATE TABLE IF NOT EXISTS `m_products` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `product_name` varchar(50) NOT NULL,
  `sku` varchar(50) NOT NULL,
  `category_id` int(10) NOT NULL,
  `basic_price` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_m_products_m_product_categories` (`category_id`),
  CONSTRAINT `FK_m_products_m_product_categories` FOREIGN KEY (`category_id`) REFERENCES `m_product_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table majoo_pos.m_products: ~3 rows (approximately)
/*!40000 ALTER TABLE `m_products` DISABLE KEYS */;
INSERT INTO `m_products` (`id`, `product_name`, `sku`, `category_id`, `basic_price`) VALUES
	(1, 'Redmi Note 10 Pro 128 GB', 'HLGM-01598', 1, 2000000),
	(2, 'Redmi Note 10 128 GB', 'HLGM-01599', 1, 3000000);
/*!40000 ALTER TABLE `m_products` ENABLE KEYS */;

-- Dumping structure for table majoo_pos.m_product_categories
CREATE TABLE IF NOT EXISTS `m_product_categories` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table majoo_pos.m_product_categories: ~2 rows (approximately)
/*!40000 ALTER TABLE `m_product_categories` DISABLE KEYS */;
INSERT INTO `m_product_categories` (`id`, `category_name`) VALUES
	(1, 'Handphone'),
	(2, 'Tablet');
/*!40000 ALTER TABLE `m_product_categories` ENABLE KEYS */;

-- Dumping structure for table majoo_pos.m_provinces
CREATE TABLE IF NOT EXISTS `m_provinces` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `prov_name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table majoo_pos.m_provinces: ~2 rows (approximately)
/*!40000 ALTER TABLE `m_provinces` DISABLE KEYS */;
INSERT INTO `m_provinces` (`id`, `prov_name`) VALUES
	(1, 'Jawa Timur'),
	(2, 'Jawa Tengah');
/*!40000 ALTER TABLE `m_provinces` ENABLE KEYS */;

-- Dumping structure for table majoo_pos.m_role_users
CREATE TABLE IF NOT EXISTS `m_role_users` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table majoo_pos.m_role_users: ~2 rows (approximately)
/*!40000 ALTER TABLE `m_role_users` DISABLE KEYS */;
INSERT INTO `m_role_users` (`id`, `role_name`) VALUES
	(1, 'admin'),
	(2, 'merchant');
/*!40000 ALTER TABLE `m_role_users` ENABLE KEYS */;

-- Dumping structure for table majoo_pos.m_stocks
CREATE TABLE IF NOT EXISTS `m_stocks` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `outlet_id` int(10) NOT NULL,
  `product_id` int(50) NOT NULL,
  `selling_price` int(10) NOT NULL,
  `qty` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_m_stocks_m_outlets` (`outlet_id`),
  KEY `FK_m_stocks_m_products` (`product_id`),
  CONSTRAINT `FK_m_stocks_m_outlets` FOREIGN KEY (`outlet_id`) REFERENCES `m_outlets` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_m_stocks_m_products` FOREIGN KEY (`product_id`) REFERENCES `m_products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table majoo_pos.m_stocks: ~0 rows (approximately)
/*!40000 ALTER TABLE `m_stocks` DISABLE KEYS */;
/*!40000 ALTER TABLE `m_stocks` ENABLE KEYS */;

-- Dumping structure for table majoo_pos.m_users
CREATE TABLE IF NOT EXISTS `m_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nick_name` varchar(50) NOT NULL,
  `mail` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_m_users_m_role_users` (`role_id`),
  CONSTRAINT `FK_m_users_m_role_users` FOREIGN KEY (`role_id`) REFERENCES `m_role_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table majoo_pos.m_users: ~3 rows (approximately)
/*!40000 ALTER TABLE `m_users` DISABLE KEYS */;
INSERT INTO `m_users` (`id`, `nick_name`, `mail`, `password`, `role_id`) VALUES
	(1, 'admin', 'admin@gmail.com', 'password', 1),
	(2, 'Telololo', 'telo@gmail.com', 'teltelo', 2),
	(5, 'budibudi', 'budibudi@mail.com', 'bams231', 2),
	(8, 'uwuwuw', 'uwu@gmail.com', 'uwu', 2);
/*!40000 ALTER TABLE `m_users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
