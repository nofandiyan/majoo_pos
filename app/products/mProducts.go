package products

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"majoo_pos/database"

	"github.com/gin-gonic/gin"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

type m_products struct {
	Id           int    `json:"id"`
	Product_name string `json:"product_name"`
	Sku          string `json:"sku"`
	Category_id  int    `json:"category_id"`
	Basic_price  int    `json:"basic_price"`
}

type Result struct {
	Code    int         `json:"code"`
	Data    interface{} `json:"data"`
	Message string      `json:"message"`
}

func ProductPage(c *gin.Context) {
	fmt.Fprintf(c.Writer, "Product!")
}

//create
func CreateProduct(c *gin.Context) {
	//get input
	payloads, _ := ioutil.ReadAll(c.Request.Body)

	var product m_products

	//json.Unmarshal read JSON data
	//payloads (JSON input) to &product (product initiate from struct)
	json.Unmarshal(payloads, &product)

	//create to db
	database.DB.Create(&product)

	res := Result{Code: 1, Data: product, Message: "Success Create Product"}
	//json.Marshal create JSON format
	result, err := json.Marshal(res)

	if err != nil {
		http.Error(c.Writer, err.Error(), http.StatusInternalServerError)
	}

	//JSON Response
	c.Writer.Header().Set("Content-type", "application/json")
	c.Writer.WriteHeader(http.StatusOK)
	c.Writer.Write(result)
}

//get all products
func GetProducts(c *gin.Context) {
	//[]m_products{} array products
	products := []m_products{}

	database.DB.Find(&products)

	res := Result{Code: 21, Data: products, Message: "Success Get Product"}
	results, err := json.Marshal(res)

	if err != nil {
		http.Error(c.Writer, err.Error(), http.StatusInternalServerError)
	}

	c.Writer.Header().Set("Content-type", "application/json")
	c.Writer.WriteHeader(http.StatusOK)
	c.Writer.Write(results)
}

//get product by param
func GetProduct(c *gin.Context) {
	var vars interface{}

	if err := c.ShouldBindJSON(&vars); err != nil {
		c.JSON(http.StatusUnprocessableEntity, "Invalid json provided")
		return
	}

	// fmt.Println(vars)

	param := vars.(map[string]interface{})

	var product m_products

	database.DB.First(&product, param)

	res := Result{Code: 22, Data: product, Message: "Success Get Product by Param"}
	results, err := json.Marshal(res)

	if err != nil {
		http.Error(c.Writer, err.Error(), http.StatusInternalServerError)
	}

	c.Writer.Header().Set("Content-type", "application/json")
	c.Writer.WriteHeader(http.StatusOK)
	c.Writer.Write(results)
}

//update
func UpdateProduct(c *gin.Context) {

	payloads, _ := ioutil.ReadAll(c.Request.Body)

	var updateProduct m_products
	json.Unmarshal(payloads, &updateProduct)

	var product m_products
	database.DB.First(&product, payloads)
	database.DB.Model(&product).Update(updateProduct)

	res := Result{Code: 3, Data: product, Message: "Success Update Product"}
	result, err := json.Marshal(res)

	if err != nil {
		http.Error(c.Writer, err.Error(), http.StatusInternalServerError)
	}

	c.Writer.Header().Set("Content-type", "application/json")
	c.Writer.WriteHeader(http.StatusOK)
	c.Writer.Write(result)
}

//delete
func DeleteProduct(c *gin.Context) {
	//get param
	var vars interface{}

	if err := c.ShouldBindJSON(&vars); err != nil {
		c.JSON(http.StatusUnprocessableEntity, "Invalid json provided")
		return
	}

	// fmt.Println(vars)

	param := vars.(map[string]interface{})

	var product m_products

	database.DB.First(&product, param)
	database.DB.Delete(product)

	res := Result{Code: 4, Message: "Success Delete Product"}
	result, err := json.Marshal(res)

	if err != nil {
		http.Error(c.Writer, err.Error(), http.StatusInternalServerError)
	}

	c.Writer.Header().Set("Content-type", "application/json")
	c.Writer.WriteHeader(http.StatusOK)
	c.Writer.Write(result)
}
