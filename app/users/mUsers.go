package users

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"majoo_pos/database"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

//method m_users
type m_users struct {
	Id        int    `json:"id"`
	Nick_name string `json:"nick_name"`
	Mail      string `json:"mail"`
	Password  string `json:"password"`
	Role_id   int    `json:"role_id"`
}

type Result struct {
	Code    int         `json:"code"`
	Data    interface{} `json:"data"`
	Message string      `json:"message"`
}

func LoginPage(c *gin.Context) {
	var vars interface{}

	if err := c.ShouldBindJSON(&vars); err != nil {
		c.JSON(http.StatusUnprocessableEntity, "Invalid json provided")
		return
	}

	var user m_users
	param := vars.(map[string]interface{})

	//compare the user from the request, with the db:
	database.DB.First(&user, param)

	if user.Id == 0 { // if not found
		c.JSON(http.StatusUnauthorized, "Please provide valid login details")
		return
	}

	token, err := CreateToken(uint64(user.Id))
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		c.Abort()
	}
	c.JSON(http.StatusOK, gin.H{
		"token": token,
	})
}

func CreateToken(userId uint64) (string, error) {
	var err error
	//Creating Access Token
	atClaims := jwt.MapClaims{}
	atClaims["authorized"] = true
	atClaims["user_id"] = userId
	atClaims["exp"] = time.Now().Add(time.Minute * 15).Unix()
	at := jwt.NewWithClaims(jwt.SigningMethodHS256, atClaims)
	token, err := at.SignedString([]byte("secret"))
	if err != nil {
		return "", err
	}
	return token, nil
}

func UserPage(c *gin.Context) {
	fmt.Fprintf(c.Writer, "User Page!")
}

//create
func CreateUser(c *gin.Context) {
	payloads, _ := ioutil.ReadAll(c.Request.Body)

	var user m_users
	json.Unmarshal(payloads, &user)

	database.DB.Create(&user)

	res := Result{Code: 1, Data: user, Message: "Success Create User"}
	result, err := json.Marshal(res)

	if err != nil {
		http.Error(c.Writer, err.Error(), http.StatusInternalServerError)
	}

	c.Writer.Header().Set("Content-type", "application/json")
	c.Writer.WriteHeader(http.StatusOK)
	c.Writer.Write(result)
}

//get all users
func GetUsers(c *gin.Context) {
	users := []m_users{}

	database.DB.Find(&users)

	res := Result{Code: 21, Data: users, Message: "Success Get Users"}
	results, err := json.Marshal(res)

	if err != nil {
		http.Error(c.Writer, err.Error(), http.StatusInternalServerError)
	}

	c.Writer.Header().Set("Content-type", "application/json")
	c.Writer.WriteHeader(http.StatusOK)
	c.Writer.Write(results)
}

//get user by param
func GetUser(c *gin.Context) {
	var vars interface{}

	if err := c.ShouldBindJSON(&vars); err != nil {
		c.JSON(http.StatusUnprocessableEntity, "Invalid json provided")
		return
	}

	// fmt.Println(vars)

	param := vars.(map[string]interface{})

	var user m_users

	database.DB.First(&user, param)

	res := Result{Code: 22, Data: user, Message: "Success Get User by Id"}
	results, err := json.Marshal(res)

	if err != nil {
		http.Error(c.Writer, err.Error(), http.StatusInternalServerError)
	}

	c.Writer.Header().Set("Content-type", "application/json")
	c.Writer.WriteHeader(http.StatusOK)
	c.Writer.Write(results)
}

//update
func UpdateUser(c *gin.Context) {

	payloads, _ := ioutil.ReadAll(c.Request.Body)

	var updateUser m_users
	json.Unmarshal(payloads, &updateUser)

	var user m_users
	database.DB.First(&user, payloads)
	database.DB.Model(&user).Update(updateUser)

	res := Result{Code: 3, Data: user, Message: "Success Update User"}
	result, err := json.Marshal(res)

	if err != nil {
		http.Error(c.Writer, err.Error(), http.StatusInternalServerError)
	}

	c.Writer.Header().Set("Content-type", "application/json")
	c.Writer.WriteHeader(http.StatusOK)
	c.Writer.Write(result)
}

//delete
func DeleteUser(c *gin.Context) {
	//get param
	var vars interface{}

	if err := c.ShouldBindJSON(&vars); err != nil {
		c.JSON(http.StatusUnprocessableEntity, "Invalid json provided")
		return
	}

	// fmt.Println(vars)

	param := vars.(map[string]interface{})

	var user m_users

	database.DB.First(&user, param)
	database.DB.Delete(user)

	res := Result{Code: 4, Message: "Success Delete User"}
	result, err := json.Marshal(res)

	if err != nil {
		http.Error(c.Writer, err.Error(), http.StatusInternalServerError)
	}

	c.Writer.Header().Set("Content-type", "application/json")
	c.Writer.WriteHeader(http.StatusOK)
	c.Writer.Write(result)
}
